﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareImagesLibrary
{
    public class Image
    {
        public const int SizeOfArgb = 4;

        public PFBitmap(int width, int height)
        {
            PixelWidth = width;
            PixelHeight = height;
            Pixels = new int[width * height];
        }
    }
}
