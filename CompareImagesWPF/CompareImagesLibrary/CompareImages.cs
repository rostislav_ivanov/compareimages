﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace CompareImagesLibrary
{
    public class CompareImages
    {
        private readonly Bitmap img1;
        private readonly Bitmap img2;
        private readonly byte n;
        private readonly byte m;

        private readonly int width;
        private readonly int height;

        public CompareImages(Bitmap img1, Bitmap img2, byte maxObjectsCount, byte errorTolerance)
        {
            this.img1 = img1 ?? throw new NullReferenceException("img1 is null");
            this.img2 = img2 ?? throw new NullReferenceException("img2 is null");
            n = maxObjectsCount;
            m = errorTolerance;

            width = Math.Min(img1.Width, img2.Width);
            height = Math.Min(img1.Height, img2.Height);

            if (width == 0) throw new NullReferenceException("width is 0");
            if (height == 0) throw new NullReferenceException("height is 0");
        }

        public List<RefRect> FindDifferences()
        {
            List<RefRect> differences = new List<RefRect>();

            Rectangle rect1 = new Rectangle(0, 0, img1.Width, img1.Height);
            BitmapData data1 = img1.LockBits(rect1, ImageLockMode.ReadWrite, img1.PixelFormat);

            Rectangle rect2 = new Rectangle(0, 0, img2.Width, img2.Height);
            BitmapData data2 = img2.LockBits(rect2, ImageLockMode.ReadWrite, img2.PixelFormat);

            //imgDiff = new Bitmap(width, height);
            //BitmapData dataResult = imgDiff.LockBits(new Rectangle(0, 0, width, height), 
            //                                         ImageLockMode.ReadWrite, img2.PixelFormat);

            unsafe
            {
                int* pixels1 = (int*)(void*)data1.Scan0;
                int* pixels2 = (int*)(void*)data2.Scan0;
                //int* pixelsResult = (int*)(void*)dataResult.Scan0;

                int pixel1, pixel2;

                bool isInsideRectangle = false, isRelatedToPreviuosRectangle = false;
                RefRect lastRect = null;

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        pixel1 = *pixels1;
                        pixel2 = *pixels2;

                        if (!ArePixelsSimilar((pixel1 >> 16) & 0xff, (pixel1 >> 8) & 0xff, pixel1 & 0xff,
                                              (pixel2 >> 16) & 0xff, (pixel2 >> 8) & 0xff, pixel2 & 0xff))
                        {
                            isRelatedToPreviuosRectangle = false;
                            for (int i = differences.Count - 1; i >= 0; i--)
                            {
                                if (differences[i].Value.Bottom == y - 1 &&
                                    differences[i].Value.Left <= x &&
                                    differences[i].Value.Right >= x)
                                {
                                    if (isInsideRectangle)
                                    {
                                        lastRect.Value = Rectangle.Union(differences[i].Value, lastRect.Value);
                                        differences.RemoveAt(i);
                                    }
                                    else
                                    {
                                        lastRect = differences[i];
                                        lastRect.Value.Height++;
                                        isInsideRectangle = true;
                                    }
                                    isRelatedToPreviuosRectangle = true;
                                    break;
                                }
                            }

                            if (!isRelatedToPreviuosRectangle)
                            {
                                if (isInsideRectangle)
                                {
                                    lastRect.Value.Width++;
                                }
                                else
                                {
                                    lastRect = new RefRect() { Value = new Rectangle(x, y, 0, 0) };
                                    differences.Add(lastRect);
                                    isInsideRectangle = true;
                                }
                            }
                        }
                        else
                        {
                            if (isInsideRectangle)
                            {
                                isInsideRectangle = false;
                            }
                        }

                        pixels1++;
                        pixels2++;
                        //pixelsResult++;
                    }
                }
            }

            img1.UnlockBits(data1);
            img2.UnlockBits(data2);
            return differences;
        }

        private bool ArePixelsSimilar(int R1, int G1, int B1, int R2, int G2, int B2)
        {
            return (Math.Abs(R1 - R2) > m || Math.Abs(G1 - G2) > m || Math.Abs(B1 - B2) > m) ? false : true;
        }

        public Bitmap DrawRectangles(Bitmap img, List<RefRect> rectangles)
        {
            Bitmap imgResult = img.Clone(new Rectangle(0, 0, width, height), img.PixelFormat);

            BitmapData data = imgResult.LockBits(new Rectangle(0, 0, width, height), 
                                        ImageLockMode.ReadWrite, imgResult.PixelFormat);

            int rectangleColor = (255 << 24) | (255 << 16);

            unsafe
            {
                int* pixels = (int*)(void*)data.Scan0;

                foreach(var rect in rectangles)
                {
                    for(int i = rect.Value.X; i <= rect.Value.Right; i++)
                    {
                        pixels[rect.Value.Y * width + i] = rectangleColor;
                        pixels[rect.Value.Bottom * width + i] = rectangleColor;
                    }

                    for (int j = rect.Value.Y; j <= rect.Value.Bottom; j++)
                    {
                        pixels[j * width + rect.Value.Y] = rectangleColor;
                        pixels[j * width + rect.Value.Right] = rectangleColor;
                    }
                }
            }

            imgResult.UnlockBits(data);

            return imgResult;
        }
    }
}
