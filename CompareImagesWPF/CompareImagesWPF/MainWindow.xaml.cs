﻿using CompareImagesLibrary;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CompareImagesWPF
{
    public partial class MainWindow : Window
    {
        Bitmap image1, image2;
        string filename1, filename2;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                var bitmapImg = new BitmapImage(new Uri(op.FileName));

                if (sender == btnLoad1)
                {
                    filename1 = op.SafeFileName;
                    image1 = BitmapImageToBitmap(bitmapImg);
                    imgPhoto1.Source = bitmapImg;
                }
                else
                {
                    filename2 = op.SafeFileName;
                    image2 = BitmapImageToBitmap(bitmapImg);
                    imgPhoto2.Source = bitmapImg;
                }

            }
        }

        private Bitmap BitmapImageToBitmap(BitmapImage bitmapImage)
        {
            using (MemoryStream outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                Bitmap bitmap = new Bitmap(outStream);

                return new Bitmap(bitmap);
            }
        }

        BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }

        CompareImages compareImages;
        private void btnLoad_Compare(object sender, RoutedEventArgs e)
        {

            byte n = 0;
            byte m = 30;
            compareImages = new CompareImages(image1, image2, 0, m);
            var result = compareImages.FindDifferences();

            Bitmap bitmapWithRects1 = compareImages.DrawRectangles(image1, result);
            imgPhoto1.Source = BitmapToImageSource(bitmapWithRects1);

            Bitmap bitmapWithRects2 = compareImages.DrawRectangles(image2, result);
            imgPhoto2.Source = BitmapToImageSource(bitmapWithRects2);
            //compareImages.FindDifferences().Save(filename1 + filename1 + m + ".jpg");
        }
        
    }
}
